﻿using Microsoft.EntityFrameworkCore;
using srvPrintshopIPEX.Data.Repositories.Interfaces;

namespace srvPrintshopIPEX.Data.Repositories
{
    public class Repository : ReadOnlyRepository, IRepository
    {
        public Repository(InforiusPrintSpoolerContext context) : base(context)
        {
        }
        public virtual void Add<TEntity>(TEntity entity) where TEntity : class
        {
            Context.Set<TEntity>().Add(entity);
        }

        public virtual void Update<TEntity>(TEntity entity) where TEntity : class
        {
            Context.Set<TEntity>().Attach(entity);
            Context.Entry(entity).State = EntityState.Modified;
        }

        public virtual void Delete<TEntity>(object id) where TEntity : class
        {
            var entity = Context.Set<TEntity>().Find(id);
            Delete(entity);
        }

        public virtual void Delete<TEntity>(TEntity entity) where TEntity : class
        {
            var dbSet = Context.Set<TEntity>();
            if (Context.Entry(entity).State == EntityState.Detached)
            {
                dbSet.Attach(entity);
            }
            dbSet.Remove(entity);
        }
    }
}
