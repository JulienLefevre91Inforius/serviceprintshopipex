﻿using System;
using System.Collections.Generic;
using System.Text;

namespace srvPrintshopIPEX.Data.Repositories.Interfaces
{
    public interface IRepository : IReadOnlyRepository
    {
        void Add<TEntity>(TEntity entity) where TEntity : class;

        void Update<TEntity>(TEntity entity) where TEntity : class;

        void Delete<TEntity>(object id) where TEntity : class;

        void Delete<TEntity>(TEntity entity) where TEntity : class;
    }
}
