﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace srvPrintshopIPEX.Data.Repositories.Interfaces
{
    public interface IReadOnlyRepository
    {
        IEnumerable<TEntity> GetAll<TEntity>(Func<IQueryable<TEntity>, IOrderedQueryable<TEntity>> orderBy = null,
            string includeProperties = null, int pageNumber = -1, int pageSize = -1)
            where TEntity : class;

        Task<IEnumerable<TEntity>> GetAllAsync<TEntity>(Func<IQueryable<TEntity>, IOrderedQueryable<TEntity>> orderBy = null,
            string includeProperties = null, int pageNumber = -1, int pageSize = -1)
            where TEntity : class;

        IEnumerable<TEntity> Get<TEntity>(Expression<Func<TEntity, bool>> filter = null,
            Func<IQueryable<TEntity>, IOrderedQueryable<TEntity>> orderBy = null,
            string includeProperties = null, int pageNumber = -1, int pageSize = -1)
            where TEntity : class;

        Task<IEnumerable<TEntity>> GetAsync<TEntity>(Expression<Func<TEntity, bool>> filter = null,
            Func<IQueryable<TEntity>, IOrderedQueryable<TEntity>> orderBy = null,
            string includeProperties = null, int pageNumber = -1, int pageSize = -1)
            where TEntity : class;

        TEntity GetOne<TEntity>(Expression<Func<TEntity, bool>> filter = null,
            Func<IQueryable<TEntity>, IOrderedQueryable<TEntity>> orderBy = null,
           string includeProperties = null)
           where TEntity : class;

        Task<TEntity> GetOneAsync<TEntity>(Expression<Func<TEntity, bool>> filter = null,
            Func<IQueryable<TEntity>, IOrderedQueryable<TEntity>> orderBy = null,
            string includeProperties = null)
            where TEntity : class;

        TEntity GetFirst<TEntity>(Expression<Func<TEntity, bool>> filter = null,
            Func<IQueryable<TEntity>, IOrderedQueryable<TEntity>> orderBy = null,
            string includeProperties = null)
            where TEntity : class;

        Task<TEntity> GetFirstAsync<TEntity>(Expression<Func<TEntity, bool>> filter = null,
            Func<IQueryable<TEntity>, IOrderedQueryable<TEntity>> orderBy = null,
            string includeProperties = null)
            where TEntity : class;

        TEntity GetById<TEntity>(object id) where TEntity : class;

        ValueTask<TEntity> GetByIdAsync<TEntity>(object id) where TEntity : class;

        int GetCount<TEntity>(Expression<Func<TEntity, bool>> filter = null) where TEntity : class;

        Task<int> GetCountAsync<TEntity>(Expression<Func<TEntity, bool>> filter = null) where TEntity : class;

        bool GetExists<TEntity>(Expression<Func<TEntity, bool>> filter = null,
            Func<IQueryable<TEntity>, IOrderedQueryable<TEntity>> orderBy = null)
            where TEntity : class;

        Task<bool> GetExistsAsync<TEntity>(Expression<Func<TEntity, bool>> filter = null,
            Func<IQueryable<TEntity>, IOrderedQueryable<TEntity>> orderBy = null)
            where TEntity : class;

        decimal? Sum<TEntity>(Expression<Func<TEntity, decimal?>> selector, Expression<Func<TEntity, bool>> filter = null) where TEntity : class;

        Task<decimal?> SumAsync<TEntity>(Expression<Func<TEntity, decimal?>> selector, Expression<Func<TEntity, bool>> filter = null) where TEntity : class;

        Task<Tuple<int, IEnumerable<dynamic>>> Select<TEntity>(string selector, Expression<Func<TEntity, bool>> filter = null,
            string orderBy = null, string includeProperties = null, int pageNumber = -1, int pageSize = -1)
              where TEntity : class;

        Task<Tuple<int, IEnumerable<dynamic>>> SelectGroupBy<TEntity>(string selectClause, string groupByClause, Expression<Func<TEntity, bool>> filter = null,
           string orderBy = null, string includeProperties = null, int pageNumber = -1, int pageSize = -1)
             where TEntity : class;
    }
}
