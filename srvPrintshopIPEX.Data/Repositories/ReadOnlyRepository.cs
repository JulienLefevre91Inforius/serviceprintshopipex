﻿using Microsoft.EntityFrameworkCore;
using srvPrintshopIPEX.Data.Repositories.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace srvPrintshopIPEX.Data.Repositories
{
    public class ReadOnlyRepository : IReadOnlyRepository
    {
        internal readonly InforiusPrintSpoolerContext Context;

        public ReadOnlyRepository(InforiusPrintSpoolerContext context)
        {
            Context = context;
        }

        protected virtual Tuple<int, IQueryable> GetQueryableWithSelect<TEntity>(
         Expression<Func<TEntity, bool>> filter = null,
           string orderBy = null,
           string includeProperties = null, int pageNumber = -1, int pageSize = -1, string selectClause = null, string groupByClause = null)
           where TEntity : class
        {
            int totalRecords = 0;

            includeProperties = includeProperties ?? string.Empty;

            IQueryable<TEntity> query = Context.Set<TEntity>();

            if (filter != null)
            {
                query = query.Where(filter);
            }

            totalRecords = query.Count();

            query = includeProperties.Split(new[] { ',' }, StringSplitOptions.RemoveEmptyEntries)
                    .Aggregate(query, (current, includeProperty) => current.Include(includeProperty));


            IQueryable selectQuery = null;

            if (!string.IsNullOrEmpty(groupByClause))
            {
                selectQuery = query.GroupBy(groupByClause);
            }
            
            if(selectQuery == null)
            {
                selectQuery = query.Select(selectClause);
            }
            else
            {
                selectQuery = selectQuery.Select(selectClause);
            }

            if (!string.IsNullOrEmpty(orderBy))
            {
                selectQuery = selectQuery.OrderBy(orderBy);
            }

            if (pageNumber != -1)
            {
                selectQuery = selectQuery.Skip((pageNumber - 1) * pageSize);
            }

            if (pageSize != -1)
            {
                selectQuery = selectQuery.Take(pageSize);
            }

            return new Tuple<int, IQueryable>(totalRecords, selectQuery);
        }

        protected virtual IQueryable<TEntity> GetQueryable<TEntity>(
          Expression<Func<TEntity, bool>> filter = null,
            Func<IQueryable<TEntity>, IOrderedQueryable<TEntity>> orderBy = null,
            string includeProperties = null, int pageNumber = -1, int pageSize = -1)
            where TEntity : class
        {
            includeProperties = includeProperties ?? string.Empty;
            IQueryable<TEntity> query = Context.Set<TEntity>();

            if (filter != null)
            {
                query = query.Where(filter);
            }

            query = includeProperties.Split(new[] { ',' }, StringSplitOptions.RemoveEmptyEntries)
                .Aggregate(query, (current, includeProperty) => current.Include(includeProperty));

            if (orderBy != null)
            {
                query = orderBy(query);
            }

            if (pageNumber != -1)
            {
                query = query.Skip((pageNumber - 1) * pageSize);
            }

            if (pageSize != -1)
            {
                query = query.Take(pageSize);
            }

            return query;
        }


        public virtual IEnumerable<TEntity> GetAll<TEntity>(
            Func<IQueryable<TEntity>, IOrderedQueryable<TEntity>> orderBy = null,
            string includeProperties = null, int pageNumber = -1, int pageSize = -1)
            where TEntity : class
        {
            return GetQueryable(null, orderBy, includeProperties, pageNumber, pageSize).ToList();
        }

        public virtual async Task<IEnumerable<TEntity>> GetAllAsync<TEntity>(
            Func<IQueryable<TEntity>, IOrderedQueryable<TEntity>> orderBy = null,
            string includeProperties = null, int pageNumber = -1, int pageSize = -1)
            where TEntity : class
        {
            return await GetQueryable(null, orderBy, includeProperties, pageNumber, pageSize).ToListAsync();
        }

        public virtual IEnumerable<TEntity> Get<TEntity>(Expression<Func<TEntity, bool>> filter = null,
            Func<IQueryable<TEntity>, IOrderedQueryable<TEntity>> orderBy = null,
            string includeProperties = null, int pageNumber = -1, int pageSize = -1)
            where TEntity : class
        {
            return GetQueryable(filter, orderBy, includeProperties, pageNumber, pageSize).ToList();
        }

        public virtual async Task<IEnumerable<TEntity>> GetAsync<TEntity>(Expression<Func<TEntity, bool>> filter = null,
            Func<IQueryable<TEntity>, IOrderedQueryable<TEntity>> orderBy = null,
            string includeProperties = null, int pageNumber = -1, int pageSize = -1)
            where TEntity : class
        {
            return await GetQueryable(filter, orderBy, includeProperties, pageNumber, pageSize).ToListAsync();
        }

        public virtual TEntity GetOne<TEntity>(Expression<Func<TEntity, bool>> filter = null,
            Func<IQueryable<TEntity>, IOrderedQueryable<TEntity>> orderBy = null,
            string includeProperties = "")
            where TEntity : class
        {
            return GetQueryable(filter, orderBy, includeProperties).SingleOrDefault();
        }

        public virtual async Task<TEntity> GetOneAsync<TEntity>(Expression<Func<TEntity, bool>> filter = null,
            Func<IQueryable<TEntity>, IOrderedQueryable<TEntity>> orderBy = null,
            string includeProperties = null)
            where TEntity : class
        {
            return await GetQueryable(filter, orderBy, includeProperties).SingleOrDefaultAsync();
        }

        public virtual TEntity GetFirst<TEntity>(Expression<Func<TEntity, bool>> filter = null,
            Func<IQueryable<TEntity>, IOrderedQueryable<TEntity>> orderBy = null,
            string includeProperties = "")
            where TEntity : class
        {
            return GetQueryable(filter, orderBy, includeProperties).FirstOrDefault();
        }

        public virtual async Task<TEntity> GetFirstAsync<TEntity>(Expression<Func<TEntity, bool>> filter = null,
            Func<IQueryable<TEntity>, IOrderedQueryable<TEntity>> orderBy = null,
            string includeProperties = null)
            where TEntity : class
        {
            return await GetQueryable(filter, orderBy, includeProperties).FirstOrDefaultAsync();
        }

        public virtual TEntity GetById<TEntity>(object id) where TEntity : class
        {
            return Context.Set<TEntity>().Find(id);
        }

        public virtual ValueTask<TEntity> GetByIdAsync<TEntity>(object id) where TEntity : class
        {
            return Context.Set<TEntity>().FindAsync(id);
        }

        public virtual int GetCount<TEntity>(Expression<Func<TEntity, bool>> filter = null)
            where TEntity : class
        {
            return GetQueryable(filter).Count();
        }

        public virtual Task<int> GetCountAsync<TEntity>(Expression<Func<TEntity, bool>> filter = null)
            where TEntity : class
        {
            return GetQueryable(filter).CountAsync();
        }

        public bool GetExists<TEntity>(Expression<Func<TEntity, bool>> filter = null,
            Func<IQueryable<TEntity>, IOrderedQueryable<TEntity>> orderBy = null) where TEntity : class
        {
            return GetQueryable(filter, orderBy).Any();
        }

        public Task<bool> GetExistsAsync<TEntity>(Expression<Func<TEntity, bool>> filter = null,
            Func<IQueryable<TEntity>, IOrderedQueryable<TEntity>> orderBy = null) where TEntity : class
        {
            return GetQueryable(filter, orderBy).AnyAsync();
        }

        public virtual decimal? Sum<TEntity>(Expression<Func<TEntity, decimal?>> selector, Expression<Func<TEntity, bool>> filter = null)
         where TEntity : class
        {
            return GetQueryable(filter).Sum(selector);
        }

        public virtual Task<decimal?> SumAsync<TEntity>(Expression<Func<TEntity, decimal?>> selector, Expression<Func<TEntity, bool>> filter = null)
            where TEntity : class
        {
            return GetQueryable(filter).SumAsync(selector);
        }

        public virtual async Task<Tuple<int, IEnumerable<dynamic>>> Select<TEntity>(string selector, Expression<Func<TEntity, bool>> filter = null,
            string orderBy = null, string includeProperties = null, int pageNumber = -1, int pageSize = -1)
            where TEntity : class
        {
            var result = GetQueryableWithSelect(filter, orderBy, includeProperties, pageNumber, pageSize, selector);
            //item1 = total records -> for pagination
            //item2 = result

            return new Tuple<int, IEnumerable<dynamic>>(result.Item1, await result.Item2.ToDynamicListAsync());
        }

        public virtual async Task<Tuple<int, IEnumerable<dynamic>>> SelectGroupBy<TEntity>(string selectClause, string groupByClause, Expression<Func<TEntity, bool>> filter = null, 
            string orderBy = null, string includeProperties = null, int pageNumber = -1, int pageSize = -1) where TEntity : class
        {
            var result = GetQueryableWithSelect(filter, orderBy, includeProperties, pageNumber, pageSize, selectClause, groupByClause);
            //item1 = total records -> for pagination
            //item2 = result
            return new Tuple<int, IEnumerable<dynamic>>(result.Item1, await result.Item2.ToDynamicListAsync());
        }
    }
}
