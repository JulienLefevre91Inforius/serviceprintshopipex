﻿using srvPrintshopIPEX.Data.Repositories.Interfaces;
using System.Threading.Tasks;

namespace srvPrintshopIPEX.Data.UnitOfWork
{
    public interface IUnitOfWork
    {
        IReadOnlyRepository ReadOnlyRepository { get; }

        IRepository Repository { get; }

        void Save();

        Task SaveAsync();
    }
}
