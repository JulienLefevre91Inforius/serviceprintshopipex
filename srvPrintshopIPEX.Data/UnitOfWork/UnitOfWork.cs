﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using srvPrintshopIPEX.Data.Repositories;
using srvPrintshopIPEX.Data.Repositories.Interfaces;

namespace srvPrintshopIPEX.Data.UnitOfWork
{
    public class UnitOfWork : IUnitOfWork, IDisposable
    {
        private bool _disposed;
        private readonly InforiusPrintSpoolerContext _context;
        private IRepository _repository;

        public IReadOnlyRepository ReadOnlyRepository => _repository ?? (_repository = new Repository(_context));

        public IRepository Repository => _repository ?? (_repository = new Repository(_context));

        public UnitOfWork(InforiusPrintSpoolerContext context)
        {
            _context = context;
        }

        protected virtual void Dispose(bool disposing)
        {
            if (!_disposed)
            {
                if (disposing)
                {
                    _context.Dispose();
                }
            }
            _disposed = true;
        }
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        public void Save()
        {
            ValidateEntities();
            _context.SaveChanges();
        }

        public async virtual Task SaveAsync()
        {
            ValidateEntities();
            await _context.SaveChangesAsync();
        }

        protected void ValidateEntities()
        {
            var entities = _context.ChangeTracker.Entries()
                   .Where(entry => entry.State == EntityState.Modified || entry.State == EntityState.Added)
                   .Select(entry => entry.Entity);

            var validationResults = new Dictionary<string, List<ValidationResult>>();
            foreach (var entity in entities)
            {
                var entityValidationResults = new List<ValidationResult>();
                if (!Validator.TryValidateObject(entity, new System.ComponentModel.DataAnnotations.ValidationContext(entity), entityValidationResults))
                {
                    // throw new ValidationException() or do whatever you want
                    validationResults.Add(entity.ToString(), entityValidationResults);
                }
            }

            if (validationResults.Any())
            {
                ThrowEnhancedValidationException(validationResults);
            }
        }

        protected void ThrowEnhancedValidationException(Dictionary<string, List<ValidationResult>> validationResults)
        {
            var errorMessages = Environment.NewLine;
            foreach (var dbEntityValidationResult in validationResults)
            {
                var entityName = $"[{dbEntityValidationResult.Key}]";
                const string indentation = "\t - ";
                var aggregatedValidationErrorMessages = dbEntityValidationResult.Value.Select(error => $"[{error.MemberNames} - {error.ErrorMessage}]")
                    .Aggregate(string.Empty, (current, validationErrorMessage) =>
                                current + Environment.NewLine + indentation + validationErrorMessage);
                errorMessages += $"{entityName}{aggregatedValidationErrorMessages}{Environment.NewLine}";
            }
            var exceptionMessage = string.Concat("Entities validation errors : ", errorMessages);

            throw new ValidationException(exceptionMessage);
        }
    }
}
