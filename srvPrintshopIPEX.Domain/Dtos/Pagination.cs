﻿using System;
using System.Collections.Generic;
using System.Text;

namespace srvPrintshopIPEX.Domain.Dtos
{
    public class Pagination
    {
        public string Result { get; }
        public int CurrentPage { get; }
        public int TotalPages { get; }
        public int PageSize { get; }
        public int TotalCount { get; }
        public int FirstPage { get { return 1; } }
        public int PreviousPage { get { return CurrentPage - 1 <= 0 ? LastPage : CurrentPage - 1; } }
        public int NextPage { get { return CurrentPage + 1 > TotalPages ? 1 : CurrentPage + 1; } }
        public int LastPage { get { return TotalPages; } }

        public Pagination(string result, int count, int pageNumber, int pageSize)
        {
            TotalCount = count;
            PageSize = pageSize;
            CurrentPage = pageNumber;
            TotalPages = (int)Math.Ceiling(count / (double)pageSize);
            Result = result;
        }
    }
}
