﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace srvPrintshopIPEX.Domain.Dtos
{
    [Serializable()]
    [XmlRoot("Document")]
    public class FichierMetadataVDLXML
    {
        [XmlElement("VDLEINCOMETABLEENTITY")]
        public TableEntite TableEntite { get; set; }
    }

    [Serializable()]
    public class TableEntite
    {
        [XmlElement("folioId")]
        public string Folioid { get; set; }

        [XmlElement("billingClassification")]
        public string BillingClassification { get; set; }

        [XmlElement("incomeTitle")]
        public string IncomeTitle { get; set; }

        [XmlElement("exercice")]
        public string Exercice { get; set; }

        [XmlElement("journalCode")]
        public string JournalCode { get; set; }

        [XmlElement("fiscalYear")]
        public string FiscalYear { get; set; }

        [XmlElement("descrPeriod")]
        public string DescrPeriod { get; set; }

        [XmlElement("year")]
        public string Year { get; set; }

        [XmlElement("settlementDate")]
        public string SettlementDate { get; set; }

        [XmlElement("executingVisaDate")]
        public string ExecutingVisaDate { get; set; }

        [XmlElement("sendingDate")]
        public string SendingDate { get; set; }

        [XmlElement("dueDate")]
        public string DueDate { get; set; }

        [XmlElement("ipexHeader")]
        public IpexHeader IpexHeader { get; set; }

        [XmlElement("VDLEINCOMELINEENTITY", typeof(LineEntite))]
        public LineEntite[] LineEntite { get; set; }
    }

    [Serializable()]
    public class IpexHeader
    {
        [XmlElement("ipexTemplate")]
        public string IpexTemplate { get; set; }

        [XmlElement("ipexAnnex")]
        public string IpexAnnex { get; set; }
    }

    [Serializable()]
    public class LineEntite
    {
        [XmlElement("folioId")]
        public string Folioid { get; set; }

        [XmlElement("taxeId")]
        public string TaxeId { get; set; }

        [XmlElement("custAccount")]
        public string CustAccount { get; set; }

        [XmlElement("division")]
        public string Division { get; set; }

        [XmlElement("amountMST")]
        public decimal? AmountMST { get; set; }

        [XmlElement("taxAddressDescription")]
        public string TaxAddressDescription { get; set; }

        [XmlElement("taxAddressStreetNumber")]
        public string TaxAddressStreetNumber { get; set; }

        [XmlElement("taxStreetCode")]
        public string TaxStreetCode { get; set; }

        [XmlElement("taxAddressPostbox")]
        public string TaxAddressPostbox { get; set; }

        [XmlElement("taxAddressStreet")]
        public string TaxAddressStreet { get; set; }

        [XmlElement("childName")]
        public string ChildName { get; set; }

        [XmlElement("licencePlate")]
        public string LicencePlate { get; set; }

        [XmlElement("externalUrl")]
        public string ExternalUrl { get; set; }

        [XmlElement("externalUrlDescription")]
        public string ExternalUrlDescription { get; set; }

        [XmlElement("budgetaryArticle")]
        public string BudgetaryArticle { get; set; }

        [XmlElement("vcs")]
        public string VCS { get; set; }

        [XmlElement("memoFieldAllFields")]
        public string MemoFieldAllFields { get; set; }

        [XmlElement("memoFieldFolio")]
        public string MemoFieldFolio { get; set; }

        [XmlElement("memoFieldPaymentRequest")]
        public string MemoFieldPaymentRequest { get; set; }

        [XmlElement("ipexLine")]
        public IpexLine IpexLine { get; set; }
    }

    [Serializable()]
    public class IpexLine
    {
        [XmlElement("division")]
        public string Division { get; set; }

        [XmlElement("amountWthtMarkup")]
        public decimal? AmountWthtMarkup { get; set; }

        [XmlElement("Lines")]
        public Lines Lines { get; set; }
    }

    [Serializable()]
    public class Lines
    {
        [XmlElement("Line", typeof(Line))]
        public Line[] Line { get; set; }
    }

    [Serializable()]
    public class Line
    {
        [XmlElement("left")]
        public string Left { get; set; }

        [XmlElement("right")]
        public string Right { get; set; }
    }
}
