﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace srvPrintshopIPEX.Domain.Dtos
{

    [Serializable()]
    [XmlRoot("Document")]
    public class FichierTierVDLXML
    {
        [XmlElement("Tiers", typeof(Tier))]
        public Tier[] Tiers { get; set; }
    }

    [Serializable()]
    public class Tier
    {
        [XmlElement("CustAccount")]
        public string CustAccount { get; set; }

        [XmlElement("Line1")]
        public string ContactUnstrucutredName { get; set; }

        [XmlElement("Line2")]
        public string ContactUnstructuredAddress { get; set; }

        [XmlElement("Line3")]
        public string ContactUnstructuredCity { get; set; }

        [XmlElement("Line4")]
        public string Line4 { get; set; }

        [XmlElement("Line5")]
        public string Line5 { get; set; }
    }
}
