﻿using System;
using System.Collections.Generic;
using System.Text;

namespace srvPrintshopIPEX.Domain.Dtos
{
    public class ParenContent
    {
        public int LParenIndex { get; set; }
        public int RParenIndex { get; set; }
        public string Content { get; set; }
        public string Master { get; set; }
    }
}