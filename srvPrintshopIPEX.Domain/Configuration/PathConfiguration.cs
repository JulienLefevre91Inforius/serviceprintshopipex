﻿using System;
using System.Collections.Generic;
using System.Text;

namespace srvPrintshopIPEX.Domain.Configuration
{
    public class PathConfiguration
    {
        public string XMLDestinationPath { get; set; }
        public string BATDestinationPath { get; set; }
        public string CourrierDestinationPath { get; set; }

        public string GetXMLDestinationPath(string codeClient, string folioId)
        {
            return XMLDestinationPath.Replace("{Client}", codeClient).Replace("{FolioId}", folioId);
        }

        public string GetBATDestinationPath(string codeClient, string folioId)
        {
            return BATDestinationPath.Replace("{Client}", codeClient).Replace("{FolioId}", folioId);
        }

        public string GetCourrierDestinationPath(string codeClient, string folioId)
        {
            return CourrierDestinationPath.Replace("{Client}", codeClient).Replace("{FolioId}", folioId);
        }
    }
}
