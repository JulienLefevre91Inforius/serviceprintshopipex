﻿// <auto-generated> This file has been auto generated by EF Core Power Tools. </auto-generated>
using System;
using System.Collections.Generic;

namespace srvPrintshopIPEX.Domain.Entities
{
    public partial class PsSpoolDetail
    {
        public int Id { get; set; }
        public int? SpoolId { get; set; }
        public int? Numero { get; set; }
        public string FilePath { get; set; }
        public string FileName { get; set; }
        public string LocalFileName { get; set; }
        public DateTime? DateUpload { get; set; }
        public int? PerforationOnPageNumber { get; set; }
        public int? NumberOfPages { get; set; }
        public string LocalHashfile { get; set; }
        public string ClientHashFile { get; set; }
        public bool? DocumentATelecharger { get; set; }

        public virtual PsSpool Spool { get; set; }
    }
}