﻿using srvPrintshopIPEX.BusinessLayer.Core;
using srvPrintshopIPEX.Domain.Dtos;
using srvPrintshopIPEX.Domain.Entities;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace srvPrintshopIPEX.BusinessLayer.Services
{
    public interface IPrintshopService : IGenericService
    {
        public Task ProcessXML();
        public Task DownloadFiles();
        public Task DownloadBonsATirer();
    }
}