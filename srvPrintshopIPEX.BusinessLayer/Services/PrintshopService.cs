﻿using Microsoft.Extensions.Logging;
using srvPrintshopIPEX.Api.Helpers;
using srvPrintshopIPEX.BusinessLayer.Core;
using srvPrintshopIPEX.BusinessLayer.Utils;
using srvPrintshopIPEX.Data.UnitOfWork;
using srvPrintshopIPEX.Domain.Configuration;
using srvPrintshopIPEX.Domain.Dtos;
using srvPrintshopIPEX.Domain.Entities;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace srvPrintshopIPEX.BusinessLayer.Services
{
    public class PrintshopService : GenericService, IPrintshopService
    {
        private static SFTPConfiguration _sFTPConfiguration;
        private static PathConfiguration _pathConfiguration;
        private static ILogger<PrintshopService> _logger;
        public PrintshopService(IUnitOfWork unitOfWork, SFTPConfiguration sFTPConfiguration, PathConfiguration pathConfiguration, ILogger<PrintshopService> logger) : base(unitOfWork)
        {
            UnitOfWork = unitOfWork;
            _sFTPConfiguration = sFTPConfiguration;
            _pathConfiguration = pathConfiguration;
            _logger = logger;
        }

        public async Task ProcessXML()
        {
            List<PsNrbfileTemplating> psNrbfileTemplatings = (await GetAsync<PsNrbfileTemplating>(QueryHelper.GetNrbfileTemplatingMissingSpool())).ToList();

            foreach (PsNrbfileTemplating psNrbfileTemplating in psNrbfileTemplatings)
            {
                try
                {
                    string xmlDestinationPath = _pathConfiguration.GetXMLDestinationPath(psNrbfileTemplating.CodeClient, psNrbfileTemplating.FolioId);
                    string xmlDestinationPathMetadataVDLXML = SFTPHelper.DownloadFile(_sFTPConfiguration, psNrbfileTemplating.CheminFichierMetadata, xmlDestinationPath);
                    string xmlDestinationPathTierVDLXML = SFTPHelper.DownloadFile(_sFTPConfiguration, psNrbfileTemplating.CheminFichierTiers, xmlDestinationPath);

                    if (String.IsNullOrEmpty(xmlDestinationPathMetadataVDLXML)) {
                        _logger.LogWarning($"ProcessXML : psNrbfileTemplating (ID:{psNrbfileTemplating.Id}) ignoré car le fichier XML metadata ({psNrbfileTemplating.CheminFichierMetadata}) n'a pas pu être téléchargé (introubvable)");
                        continue;
                    }

                    if (String.IsNullOrEmpty(xmlDestinationPathTierVDLXML))
                    {
                        _logger.LogWarning($"ProcessXML : psNrbfileTemplating (ID:{psNrbfileTemplating.Id}) ignoré car le fichier XML tiers ({psNrbfileTemplating.CheminFichierTiers}) n'a pas pu être téléchargé (introubvable)");
                        continue;
                    }

                    FichierMetadataVDLXML fichierMetadataVDLXML = null;
                    XmlSerializer serializerMetadata = new(typeof(FichierMetadataVDLXML));
                    StreamReader readerMetadata = new(xmlDestinationPathMetadataVDLXML);
                    fichierMetadataVDLXML = (FichierMetadataVDLXML)serializerMetadata.Deserialize(readerMetadata);
                    readerMetadata.Close();

                    FichierTierVDLXML fichierTierVDLXML = null;
                    XmlSerializer serializerTier = new(typeof(FichierTierVDLXML));
                    StreamReader readerTier = new(xmlDestinationPathTierVDLXML);
                    fichierTierVDLXML = (FichierTierVDLXML)serializerTier.Deserialize(readerTier);
                    readerTier.Close();

                    if (psNrbfileTemplating.NombreSpools != fichierMetadataVDLXML.TableEntite.LineEntite.Length)
                    {
                        _logger.LogWarning($"ProcessXML : psNrbfileTemplating (ID:{psNrbfileTemplating.Id}) ignoré car le nombre de courriers annoncés ({psNrbfileTemplating.NombreSpools}) est différent du nombre de courriers reçus " +
                            $"dans le fichier XML metadata ({fichierMetadataVDLXML.TableEntite.LineEntite.Length})");
                        continue;
                    }
                    
                    if(psNrbfileTemplating.NombreSpools != fichierTierVDLXML.Tiers.Length)
                    {
                        _logger.LogWarning($"ProcessXML : psNrbfileTemplating (ID:{psNrbfileTemplating.Id}) ignoré car le nombre de courriers annoncés ({psNrbfileTemplating.NombreSpools}) est différent du nombre d'adresses reçues " +
                            $"dans le fichier XML tiers ({fichierTierVDLXML.Tiers.Length})");
                        continue;
                    }

                    if (fichierMetadataVDLXML.TableEntite.Folioid != psNrbfileTemplating.FolioId)
                    {
                        _logger.LogWarning($"ProcessXML : psNrbfileTemplating (ID:{psNrbfileTemplating.Id}) ignoré car le FolioId annoncé ({psNrbfileTemplating.FolioId}) est différent du FolioId présent dans le fichier metadata "+
                            $"({fichierMetadataVDLXML.TableEntite.Folioid}) ");
                        continue;
                    }

                    Client client = await GetFirstAsync<Client>(QueryHelper.GetClient(psNrbfileTemplating.CodeClient));
                    if (client == null)
                    {
                        _logger.LogWarning($"ProcessXML : psNrbfileTemplating (ID:{psNrbfileTemplating.Id}) ignoré car aucun client correspondant au code ({psNrbfileTemplating.CodeClient}) est présent en base de données");
                        continue;
                    }

                    foreach (LineEntite lineEntite in fichierMetadataVDLXML.TableEntite.LineEntite)
                    { 
                        PsSpool spoolOld = await GetFirstAsync<PsSpool>(QueryHelper.GetSpool(lineEntite.TaxeId, lineEntite.CustAccount, psNrbfileTemplating.NrbfileId));
                        if (spoolOld != null)
                        {
                            _logger.LogWarning($"ProcessXML : Courrier ignoré (TaxeId:{lineEntite.TaxeId}, CustAccount:{lineEntite.CustAccount}) car un spool (ID:{spoolOld.Id}) existe déjà avec ces références pour le lot "+
                                $"(NRBFileId: {psNrbfileTemplating.NrbfileId})");
                            continue;
                        }

                        Tier tier = fichierTierVDLXML.Tiers.FirstOrDefault(x => x.CustAccount.ToLower().Equals(lineEntite.CustAccount.ToLower()));
                        if (tier == null)
                        {
                            _logger.LogWarning($"ProcessXML : Courrier ignoré (TaxeId:{lineEntite.TaxeId}, CustAccount:{lineEntite.CustAccount}) car aucune adresse correspondant au CustAccount n'a été trouvée dans le fichier XML tiers");
                            continue;
                        }

                        PsSpool psSpool = new()
                        {
                            Description = fichierMetadataVDLXML.TableEntite.IncomeTitle,
                            CustomerId = client.Id,
                            NombreAnnexes = 1,
                            CodeApplication = "IPEX",
                            ExternalReference = lineEntite.TaxeId,
                            ContactUnstructuredName = tier.ContactUnstrucutredName,
                            Country = "BELGIQUE",
                            UnstructuredPostalCodeCity = tier.ContactUnstructuredCity,
                            UnstructuredAddress = tier.ContactUnstructuredAddress,
                            NrbfileId = psNrbfileTemplating.NrbfileId,
                            StatutId = 7,
                            Vcs = lineEntite.VCS,
                            Montant = lineEntite.AmountMST,
                            ExternalContactAccount = lineEntite.CustAccount
                        };

                        UnitOfWork.Repository.Add<PsSpool>(psSpool);
                    }
                    await UnitOfWork.SaveAsync();
                }
                catch (Exception ex)
                {
                    _logger.LogWarning($"ProcessXML : Exception non gérée : {ex.InnerException}");
                }
            }
        }

        public async Task DownloadFiles()
        {
            List<PsSpoolDetail> psSpoolDetails = (await GetAsync<PsSpoolDetail>(QueryHelper.GetSpoolDetailsToDownload(),includeProperties:$"{nameof(PsSpoolDetail.Spool)}.{nameof(PsSpool.Nrbfile)}.{nameof(PsNrbfile.PsNrbfileTemplating)}")).ToList();

            foreach (PsSpoolDetail psSpoolDetail in psSpoolDetails)
            {
                try
                {
                    string courrierDestinationFolderPath = _pathConfiguration.GetCourrierDestinationPath(psSpoolDetail.Spool.Nrbfile.PsNrbfileTemplating.CodeClient, psSpoolDetail.Spool.Nrbfile.PsNrbfileTemplating.FolioId);
                    string courrierDestinationPath = SFTPHelper.DownloadFile(_sFTPConfiguration, psSpoolDetail.FilePath, courrierDestinationFolderPath);

                    if (String.IsNullOrEmpty(courrierDestinationPath))
                    {
                        _logger.LogWarning($"DownloadFiles : psSpoolDetail (ID:{psSpoolDetail.Id}) ignoré car le fichier PDF ({psSpoolDetail.FilePath}) n'a pas pu être téléchargé (introubvable)");
                        continue;
                    }

                    psSpoolDetail.DocumentATelecharger = false;

                    UnitOfWork.Repository.Update<PsSpoolDetail>(psSpoolDetail);

                    await UnitOfWork.SaveAsync();
                }
                catch (Exception ex)
                {
                    _logger.LogWarning($"ProcessXML : Exception non gérée : {ex.InnerException}");
                }
            }
        }

        public async Task DownloadBonsATirer()
        {
            List<PsNrbfileTemplating> psNrbfileTemplatings = (await GetAsync<PsNrbfileTemplating>(QueryHelper.GetNrbfileTemplatingMissingBonATirer())).ToList();

            foreach (PsNrbfileTemplating psNrbfileTemplating in psNrbfileTemplatings)
            {
                try
                {
                    string batDestinationFolderPath = _pathConfiguration.GetBATDestinationPath(psNrbfileTemplating.CodeClient, psNrbfileTemplating.FolioId);
                    string batDestinationPath = SFTPHelper.DownloadFile(_sFTPConfiguration, psNrbfileTemplating.CheminFichierBonATirer, batDestinationFolderPath);

                    if (String.IsNullOrEmpty(batDestinationPath))
                    {
                        _logger.LogWarning($"DownloadBonsATirer : psNrbfileTemplating (ID:{psNrbfileTemplating.Id}) ignoré car le bon à tirer ({psNrbfileTemplating.CheminFichierBonATirer}) n'a pas pu être téléchargé (introubvable)");
                        continue;
                    }

                    psNrbfileTemplating.BonATirerATelecharger = false;

                    UnitOfWork.Repository.Update<PsNrbfileTemplating>(psNrbfileTemplating);

                    await UnitOfWork.SaveAsync();
                }
                catch (Exception ex)
                {
                    _logger.LogWarning($"ProcessXML : Exception non gérée : {ex.InnerException}");
                }
            }
        }
    }
}
