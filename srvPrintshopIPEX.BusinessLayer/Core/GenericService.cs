﻿using srvPrintshopIPEX.Data.UnitOfWork;

namespace srvPrintshopIPEX.BusinessLayer.Core
{
    public class GenericService : GenericReadOnlyService, IGenericService
    {
        public GenericService(IUnitOfWork httpUnitOfWork) : base(httpUnitOfWork)
        {
            UnitOfWork = httpUnitOfWork;
        }

        /// <summary>
        /// Add the entity and save database context changes (HttpUnitOfWork save method call).
        /// </summary>
        /// <typeparam name="TEntity">The entity.</typeparam>
        /// <param name="entity"></param>
        public virtual void Add<TEntity>(TEntity entity) where TEntity : class
        {
            UnitOfWork.Repository.Add(entity);
            UnitOfWork.Save();
        }

        /// <summary>
        /// Update the entity and save database context changes (HttpUnitOfWork save method call).
        /// </summary>
        /// <typeparam name="TEntity">The entity.</typeparam>
        /// <param name="entity"></param>
        public virtual void Update<TEntity>(TEntity entity) where TEntity : class
        {
            UnitOfWork.Repository.Update(entity);
            UnitOfWork.Save();
        }

        /// <summary>
        /// Delete the entity by id and save database context changes (HttpUnitOfWork save method call).
        /// </summary>
        /// <typeparam name="TEntity">The entity id.</typeparam>
        /// <param name="id"></param>
        public virtual void Delete<TEntity>(object id) where TEntity : class
        {
            UnitOfWork.Repository.Delete<TEntity>(id);
            UnitOfWork.Save();
        }

        /// <summary>
        /// Delete the entity and save database context changes (HttpUnitOfWork save method call).
        /// </summary>
        /// <typeparam name="TEntity">The entity.</typeparam>
        /// <param name="entity"></param>
        public virtual void Delete<TEntity>(TEntity entity) where TEntity : class
        {
            UnitOfWork.Repository.Delete(entity);
            UnitOfWork.Save();
        }

        public void Save()
        {
            UnitOfWork.Save();
        }
    }
}
