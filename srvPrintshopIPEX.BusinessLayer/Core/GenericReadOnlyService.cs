﻿using Newtonsoft.Json;
using srvPrintshopIPEX.Data.UnitOfWork;
using srvPrintshopIPEX.Domain.Dtos;
using srvPrintshopIPEX.BusinessLayer.Utils;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace srvPrintshopIPEX.BusinessLayer.Core
{
    public class GenericReadOnlyService : IGenericReadOnlyService
    {
        internal IUnitOfWork UnitOfWork { get; set; }

        public GenericReadOnlyService(IUnitOfWork httpUnitOfWork)
        {
            UnitOfWork = httpUnitOfWork;
        }

        public virtual IEnumerable<TEntity> GetAll<TEntity>(Func<IQueryable<TEntity>, IOrderedQueryable<TEntity>> orderBy = null,
            string includeProperties = null, int pageNumber = -1, int pageSize = -1)
            where TEntity : class
        {
            return UnitOfWork.ReadOnlyRepository.GetAll(orderBy, includeProperties, pageNumber, pageSize);
        }

        public virtual async Task<IEnumerable<TEntity>> GetAllAsync<TEntity>(Func<IQueryable<TEntity>, IOrderedQueryable<TEntity>> orderBy = null,
            string includeProperties = null, int pageNumber = -1, int pageSize = -1)
            where TEntity : class
        {
            return await UnitOfWork.ReadOnlyRepository.GetAllAsync(orderBy, includeProperties, pageNumber, pageSize);

        }

        public virtual IEnumerable<TEntity> Get<TEntity>(Expression<Func<TEntity, bool>> filter = null, Func<IQueryable<TEntity>, IOrderedQueryable<TEntity>> orderBy = null,
            string includeProperties = null, int pageNumber = -1, int pageSize = -1)
            where TEntity : class
        {
            return UnitOfWork.ReadOnlyRepository.Get(filter, orderBy, includeProperties, pageNumber, pageSize);
        }

        public virtual async Task<IEnumerable<TEntity>> GetAsync<TEntity>(Expression<Func<TEntity, bool>> filter = null,
            Func<IQueryable<TEntity>, IOrderedQueryable<TEntity>> orderBy = null, string includeProperties = null,
            int pageNumber = -1, int pageSize = -1)
            where TEntity : class
        {
            return await UnitOfWork.ReadOnlyRepository.GetAsync(filter, orderBy, includeProperties, pageNumber, pageSize);
        }

        public virtual TEntity GetOne<TEntity>(Expression<Func<TEntity, bool>> filter = null, Func<IQueryable<TEntity>, IOrderedQueryable<TEntity>> orderBy = null,
            string includeProperties = null)
            where TEntity : class
        {
            return UnitOfWork.ReadOnlyRepository.GetOne(filter, orderBy, includeProperties);
        }

        public virtual async Task<TEntity> GetOneAsync<TEntity>(Expression<Func<TEntity, bool>> filter = null, Func<IQueryable<TEntity>, IOrderedQueryable<TEntity>> orderBy = null,
            string includeProperties = null)
            where TEntity : class
        {
            return await UnitOfWork.ReadOnlyRepository.GetOneAsync(filter, orderBy, includeProperties);
        }

        public virtual TEntity GetFirst<TEntity>(Expression<Func<TEntity, bool>> filter = null, Func<IQueryable<TEntity>, IOrderedQueryable<TEntity>> orderBy = null,
            string includeProperties = null)
            where TEntity : class
        {
            return UnitOfWork.ReadOnlyRepository.GetFirst(filter, orderBy, includeProperties);
        }

        public virtual async Task<TEntity> GetFirstAsync<TEntity>(Expression<Func<TEntity, bool>> filter = null, Func<IQueryable<TEntity>, IOrderedQueryable<TEntity>> orderBy = null,
            string includeProperties = null)
            where TEntity : class
        {
            return await UnitOfWork.ReadOnlyRepository.GetFirstAsync(filter, orderBy, includeProperties);
        }

        public virtual TEntity GetById<TEntity>(object id)
            where TEntity : class
        {
            return UnitOfWork.ReadOnlyRepository.GetById<TEntity>(id);
        }

        public virtual async Task<TEntity> GetByIdAsync<TEntity>(object id)
            where TEntity : class
        {
            return await UnitOfWork.ReadOnlyRepository.GetByIdAsync<TEntity>(id);
        }

        public virtual int GetCount<TEntity>(Expression<Func<TEntity, bool>> filter = null)
            where TEntity : class
        {
            return UnitOfWork.ReadOnlyRepository.GetCount(filter);
        }

        public virtual async Task<int> GetCountAsync<TEntity>(Expression<Func<TEntity, bool>> filter = null) where TEntity : class
        {
            return await UnitOfWork.ReadOnlyRepository.GetCountAsync(filter);
        }

        public virtual bool GetExists<TEntity>(Expression<Func<TEntity, bool>> filter = null, Func<IQueryable<TEntity>, IOrderedQueryable<TEntity>> orderBy = null)
            where TEntity : class
        {
            return UnitOfWork.ReadOnlyRepository.GetExists(filter, orderBy);
        }

        public virtual async Task<bool> GetExistsAsync<TEntity>(Expression<Func<TEntity, bool>> filter = null, Func<IQueryable<TEntity>, IOrderedQueryable<TEntity>> orderBy = null)
            where TEntity : class
        {
            return await UnitOfWork.ReadOnlyRepository.GetExistsAsync(filter);
        }

        public virtual decimal? Sum<TEntity>(Expression<Func<TEntity, decimal?>> selector, Expression<Func<TEntity, bool>> filter = null)
            where TEntity : class
        {
            return UnitOfWork.ReadOnlyRepository.Sum(selector, filter);
        }

        public virtual async Task<decimal?> SumAsync<TEntity>(Expression<Func<TEntity, decimal?>> selector, Expression<Func<TEntity, bool>> filter = null)
            where TEntity : class
        {
            return await UnitOfWork.ReadOnlyRepository.SumAsync(selector, filter);
        }

        public virtual async Task<Pagination> Select<TEntity>(string fields, Dictionary<string, string> mappings, Expression<Func<TEntity, bool>> filter = null, string orderBy = null,
            string includeProperties = null, int pageNumber = -1, int pageSize = -1, Dictionary<string, string> nestedProperties = null)
            where TEntity : class
        {
            //Example of fields = id,files(id,number,path,spoolid,name,uploadDate),files.NumberOfPagesexternalid,color(id,code)

            //If fields is null or empty then add all fields
            if (string.IsNullOrEmpty(fields))
            {
                fields = String.Join(",", mappings.Select(dt => dt.Key));
            }

            //Transform fields into string with each individual selector
            //Example: 
            //        - Fields = id,files(id,number,path,spoolid,name,uploadDate),files.NumberOfPagesexternalid,color(id,code)
            //        - After transformation = id,files.id,files.number,files.path,files.spoolid,files.name,files.uploadDate,files.NumberOfPagesexternalid,color.id,color.code
            string stringSelector = FieldsHelper.TransformFieldsIntoStringWithEachIndividualSelector(fields);

            //Get concrete order by
            string concreteOrderBy = SelectorHelper.GetConcreteOrderBy(orderBy, mappings);

            if (!string.IsNullOrEmpty(concreteOrderBy))
            {
                //If a column is specified in the orderBy clause then it needs to be in the fields...
                string orderByFields = SelectorHelper.GetOrderByFields(concreteOrderBy);

                string[] selectorsArray = stringSelector.Split(",");

                foreach (string orderElement in orderByFields.Split(","))
                {
                    //find mapping
                    var map = mappings.FirstOrDefault(m => m.Value.ToLowerInvariant().Equals(orderElement.ToLowerInvariant()));

                    if (map.Key != null && selectorsArray.FirstOrDefault(s => s.ToLowerInvariant().Equals(map.Key.ToLowerInvariant())) == null)
                    {
                        if (string.IsNullOrEmpty(stringSelector))
                        {
                            stringSelector = map.Key;
                        }
                        else
                        {
                            stringSelector = string.Join(",", stringSelector, map.Key);
                        }
                    }
                }
            }

            //Get a list of all individual concrete (column is existing in table) selectors (split based on character ",") with an alias
            //Based on mapping from Fields.DestinationToSource dictionary
            //Examples:
            //        - List[0] = "Id as Id" (mapping "Id" => "Id")
            //        - List[1] = "SpoolDetail.Id as SpoolDetail_Id" (mapping "Files.Id" => "SpoolDetail.Id")
            //        - List[2] = "SpoolDetail.Numero as SpoolDetail_Numero" (mapping "Files.Number" => "SpoolDetail.Numero")
            //        - ...
            List<string> allSelectors = SelectorHelper.GetAllConcreteSelectorsAsList(stringSelector, mappings);

            //Init final string value to return
            string stringFinalSelectors = string.Empty;

            //Check if nested properties are configured (Selectors.NestedProperties dictionary)
            if (nestedProperties != null && nestedProperties.Count > 0)
            {
                //Get all non nested selectors (if any) from the list
                //Examples:
                //         -  List[0] = "Id as Id"
                //         -  List[1] = "Color.Id as Color_Id"
                //         -  List[2] = "Color.Code as Color_Code"
                List<string> keysToSearch = nestedProperties.Select(np => np.Key.ToLowerInvariant() + ".").ToList();
                List<string> backupAllSelectors = allSelectors;
                List<string> nonNestedSelectors = new();

                foreach (string key in keysToSearch)
                {
                    backupAllSelectors = backupAllSelectors.Where(s => !s.ToLowerInvariant().Contains(key)).ToList();
                }

                nonNestedSelectors = backupAllSelectors;

                //Transform already non nested selectors to string (string used by Dynamic.Core library)
                //Example:
                //          - stringFinalSelectors = new(Id as Id,Color.Id as Color_Id,Color.Code as Color_Code)
                stringFinalSelectors = SelectorHelper.TransformSelectorsToString(nonNestedSelectors);

                //Loop dictionary of nested properties 
                //Example:
                //        - { "SpoolDetail", "Files" }
                //          -> SpoolDetail is the nested property and Files is an alias
                foreach (var np in nestedProperties)
                {
                    //Get key (example = "spooldetail")
                    string key = np.Key.ToLowerInvariant();

                    //Concat character "." to key (example = "spooldetail.")
                    string keyToSearch = key + ".";

                    //Get nested selectors (if any) from the list + omit SpoolDetail (see split based on character '.')
                    //Examples:
                    //         -  List[0] = "Id as SpoolDetail_Id" 
                    //         -  List[1] = "Numero as SpoolDetail_Numero"
                    //         -  List[2] = "FilePath as SpoolDetail_FilePath"
                    //         -  List[3] = "SpoolId as SpoolDetail_SpoolId"
                    //         -  List[4] = "FileName as SpoolDetail_FileName"
                    //         -  List[5] = "DateUpload as SpoolDetail_DateUpload"
                    List<string> nestedSelectors = allSelectors.Where(s => s.ToLowerInvariant().Contains(keyToSearch)).Select(s => s.Split(".")[1]).ToList();

                    //Check if there is at least one nested selector
                    if (nestedSelectors.Count > 0)
                    {
                        //Transform nested selectors to string (string used by Dynamic.Core library)
                        //Example:
                        //          - stringNestedSelectors = new(Id as SpoolDetail_Id,Numero as SpoolDetail_Numero,FilePath as SpoolDetail_FilePath,SpoolId as SpoolDetail_SpoolId,FileName as SpoolDetail_FileName,DateUpload as SpoolDetail_DateUpload)
                        string stringNestedSelectors = SelectorHelper.TransformSelectorsToString(nestedSelectors);

                        //Above string is not yet complete to handle nested properties... (specific to Dynamic.Core library)
                        //Example:
                        //         - stringNestedSelectors = spooldetail.Select(new(Id as SpoolDetail_Id,Numero as SpoolDetail_Numero,FilePath as SpoolDetail_FilePath,SpoolId as SpoolDetail_SpoolId,FileName as SpoolDetail_FileName,DateUpload as SpoolDetail_DateUpload)) as Files
                        stringNestedSelectors = SelectorHelper.TransformNestedSelectorsToString(stringNestedSelectors, key, np.Value);

                        //Add this string to the final string (at the end, just before ')')
                        //Example:
                        //         - stringFinalSelectors = new(Id as Id,Color.Id as Color_Id,Color.Code as Color_Code,spooldetail.Select(new(Id as SpoolDetail_Id,Numero as SpoolDetail_Numero,FilePath as SpoolDetail_FilePath,SpoolId as SpoolDetail_SpoolId,FileName as SpoolDetail_FileName,DateUpload as SpoolDetail_DateUpload)) as Files)
                        stringFinalSelectors = stringFinalSelectors.Insert(stringFinalSelectors.Length - 1, (nonNestedSelectors.Count > 0 ? "," : "") + stringNestedSelectors);
                    }
                }
            }
            else
            {
                //Init a list with all properties to reject (nested)
                List<string> propertiesToReject = new();

                //Get all IEnumerable properties
                foreach (var prop in typeof(TEntity).GetProperties().Where(p => p.PropertyType.GetInterfaces().Any(x => x == typeof(IEnumerable))))
                {
                    //string is considered as IEnumerable
                    if (!prop.PropertyType.FullName.ToLowerInvariant().Equals("System.String".ToLowerInvariant()))
                    {
                        propertiesToReject.Add(prop.Name.ToLowerInvariant() + ".");
                    }
                }

                foreach (var pt in propertiesToReject)
                {
                    //filter all selectors with the property to reject
                    allSelectors = allSelectors.Where(s => !s.ToLowerInvariant().Contains(pt)).ToList();
                }

                stringFinalSelectors = SelectorHelper.TransformSelectorsToString(allSelectors);
            }

            //Let's go!
            var result = await UnitOfWork.ReadOnlyRepository.Select(stringFinalSelectors, filter, concreteOrderBy, includeProperties, pageNumber, pageSize);

            return new Pagination(SelectorHelper.GetFinalResult(JsonConvert.SerializeObject(result.Item2), allSelectors, mappings), result.Item1, pageNumber, pageSize);
        }

        public async Task<IEnumerable<dynamic>> SelectWithGroupBy<TEntity>(string selectClause, string groupByClause, Expression<Func<TEntity, bool>> filter = null, string orderBy = null, string includeProperties = null, 
            int pageNumber = -1, int pageSize = -1) where TEntity : class
        {
            var result = await UnitOfWork.ReadOnlyRepository.SelectGroupBy(selectClause, groupByClause, filter, orderBy, includeProperties, pageNumber, pageSize);

            return result.Item2; 
        }
    }
}
