﻿
namespace srvPrintshopIPEX.BusinessLayer.Core
{

    public interface IGenericService : IGenericReadOnlyService
    {
        /// <summary>
        /// Add the entity and save database context changes (UnitOfWork save method call).
        /// </summary>
        /// <typeparam name="TEntity">The entity.</typeparam>
        /// <param name="entity"></param>
        void Add<TEntity>(TEntity entity) where TEntity : class;

        /// <summary>
        /// Update the entity and save database context changes (UnitOfWork save method call).
        /// </summary>
        /// <typeparam name="TEntity">The entity.</typeparam>
        /// <param name="entity"></param>
        void Update<TEntity>(TEntity entity) where TEntity : class;

        /// <summary>
        /// Delete the entity by id and save database context changes (UnitOfWork save method call).
        /// </summary>
        /// <typeparam name="TEntity">The entity id.</typeparam>
        /// <param name="id"></param>
        void Delete<TEntity>(object id) where TEntity : class;

        /// <summary>
        /// Delete the entity and save database context changes (UnitOfWork save method call).
        /// </summary>
        /// <typeparam name="TEntity">The entity.</typeparam>
        /// <param name="entity"></param>
        void Delete<TEntity>(TEntity entity) where TEntity : class;

        void Save();
    }
}
