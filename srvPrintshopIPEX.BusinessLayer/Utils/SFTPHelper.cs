﻿using Renci.SshNet;
using srvPrintshopIPEX.Domain.Configuration;
using System.IO;
using System.Linq;
using System.Xml.Serialization;

namespace srvPrintshopIPEX.BusinessLayer.Utils
{
    public static class SFTPHelper
    {
        public static string DownloadFile(SFTPConfiguration sFTPConfiguration, string sourceFilePath, string destinationFolderPath)
        {
            using (var client = new SftpClient(sFTPConfiguration.Host, sFTPConfiguration.Port, sFTPConfiguration.User, sFTPConfiguration.Password))
            {
                client.Connect();
                var file = client.ListDirectory(Path.GetDirectoryName(sourceFilePath)).FirstOrDefault(x => x.Name == Path.GetFileName(sourceFilePath));
                if (file != null)
                {
                    if(!Directory.Exists(destinationFolderPath))
                        Directory.CreateDirectory(destinationFolderPath);  

                    using (Stream fileStream = File.OpenWrite(Path.Combine(destinationFolderPath, file.Name)))
                    {
                        client.DownloadFile(file.FullName, fileStream);
                    }
                    return Path.Combine(destinationFolderPath, file.Name);
                }
                else
                    return string.Empty;
            }
        }
    }
}
