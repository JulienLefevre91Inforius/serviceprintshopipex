﻿using srvPrintshopIPEX.Domain.Dtos;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;

namespace srvPrintshopIPEX.BusinessLayer.Utils
{
    public static class FieldsHelper
    {
        public static string TransformFieldsIntoStringWithEachIndividualSelector(string fields)
        {
            if (string.IsNullOrEmpty(fields))
            {
                return string.Empty;
            }

            //Remove space...
            fields = Regex.Replace(fields, @"\s+", "");
            //Replace all / by .
            fields = fields.Replace("/", ".");

            //Build a list of content
            List<ParenContent> listParens = new();
            //Count numbers of LParen in string
            int totalLParens = fields.Count(x => x == '(');

            if (totalLParens > 0)
            {
                //Searching
                int startIndexLParen = 0;
                int startIndexRParen = fields.Length - 1;

                for (int i = 0; i < totalLParens; i++)
                {
                    //Find the first occurence/position of LParen in string
                    int openParenIndex = fields.IndexOf("(", startIndexLParen);
                    //Init occurence/position of close RParen in string
                    int closeParenIndex = openParenIndex;

                    //If nested LParen then we need to find the correct RParen
                    int byPassRParen = 0;
                    //Counter
                    int currentIndex = 0;

                    //Keep only what we need...
                    string strToLoop = fields.Substring(openParenIndex + 1, fields.Length - openParenIndex - 1);

                    foreach (char c in strToLoop)
                    {
                        //If find nested LParen we know we have to skip the next RParen
                        if (c.Equals('('))
                        {
                            byPassRParen++;
                        }
                        else if (c.Equals(')'))
                        {
                            if (byPassRParen > 0)
                            {
                                //Not the one we are searching for...
                                byPassRParen--;
                            }
                            else if (byPassRParen == 0)
                            {
                                closeParenIndex = currentIndex + openParenIndex;
                                break;
                            }
                        }
                        currentIndex++;
                    }

                    //Master is the one which needs to be distributed
                    string[] masters = fields.Substring(0, openParenIndex).Split(',');

                    listParens.Add(new ParenContent
                    {
                        LParenIndex = openParenIndex,
                        RParenIndex = closeParenIndex,
                        Content = fields.Substring(openParenIndex + 1, closeParenIndex - openParenIndex),
                        Master = masters[masters.Length - 1]
                    });

                    startIndexLParen = openParenIndex + 1;
                    startIndexRParen = closeParenIndex - 1;
                }
            }

            string finalFields = fields;

            //Replace process (old value to new value)
            for (int i = listParens.Count - 1; i >= 0; i--)
            {
                var parant = listParens[i];
                string[] items = parant.Content.Split(',');

                string newValue = string.Empty;

                foreach (string item in items)
                {
                    newValue += string.Concat(parant.Master, ".", item, ",");
                }

                string oldValue = string.Concat(parant.Master, "(", parant.Content, ")");
                finalFields = finalFields.Replace(oldValue, newValue.Trim(','));

                int j = i;

                while (j - 1 >= 0)
                {
                    listParens[j - 1].Content = listParens[j - 1].Content.Replace(oldValue, newValue.Trim(','));
                    j--;
                }
            }

            return finalFields;
        }
    }
}
