﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using srvPrintshopIPEX.BusinessLayer.Mappings;
using System;
using System.Collections.Generic;
using System.Linq;

namespace srvPrintshopIPEX.BusinessLayer.Utils
{
    public static class SelectorHelper
    {
        public static string GetOrderByFields(string stringOrderBy)
        {
            string orderByFields = string.Empty;

            foreach (string orderElement in stringOrderBy.Split(","))
            {
                string element = orderElement.Replace("descending", string.Empty).Trim();

                if (string.IsNullOrEmpty(orderByFields))
                {
                    orderByFields = element;
                }
                else
                {
                    orderByFields = string.Join(",", orderByFields, element);
                }
            }

            return orderByFields;
        }

        public static string GetConcreteOrderBy(string stringOrderBy, Dictionary<string, string> mappings)
        {
            if (string.IsNullOrEmpty(stringOrderBy))
            {
                return null;
            }

            string finalOrderBy = string.Empty;

            //?sortFields=rating,reviews,name;sortOrder=asc 
            if (stringOrderBy.Contains(";"))
            {
                string[] stringOrderBySplitted = stringOrderBy.Split(";");

                foreach (string element in stringOrderBySplitted[0].Split(","))
                {
                    var map = mappings.FirstOrDefault(m => m.Key.ToLowerInvariant().Equals(element.ToLowerInvariant()));

                    if (map.Key != null)
                    {
                        string toAdd = map.Value;

                        if (stringOrderBySplitted[1].ToLowerInvariant().Equals("sortorder=desc"))
                        {
                            toAdd += " descending";
                        }

                        if (string.IsNullOrEmpty(finalOrderBy))
                        {
                            finalOrderBy = toAdd;
                        }
                        else
                        {
                            finalOrderBy = string.Join(",", finalOrderBy, toAdd);
                        }


                    }
                }
            }
            else
            {
                //?sort=+rating,-reviews,+name 
                string[] stringOrderBySplitted = stringOrderBy.Split(",");

                foreach (string element in stringOrderBySplitted)
                {
                    bool descending = element.Contains("-");
                    string clearElement = element.Trim('-').Trim('+');

                    var map = mappings.FirstOrDefault(m => m.Key.ToLowerInvariant().Equals(clearElement.ToLowerInvariant()));

                    if (map.Key != null)
                    {
                        string toAdd = map.Value;

                        if (descending)
                        {
                            toAdd += " descending";
                        }

                        if (string.IsNullOrEmpty(finalOrderBy))
                        {
                            finalOrderBy = toAdd;
                        }
                        else
                        {
                            finalOrderBy = string.Join(",", finalOrderBy, toAdd);
                        }
                    }
                }
            }

            return finalOrderBy;
        }


        public static List<string> GetAllConcreteSelectorsAsList(string stringSelector, Dictionary<string, string> mappings)
        {
            //Build a string list
            List<string> selectors = new();

            //Loop individual selector
            foreach (string field in stringSelector.Split(','))
            {
                //Check if mapping is existing
                string value = mappings.FirstOrDefault(x => x.Key.ToLowerInvariant().Equals(field.ToLowerInvariant())).Value;

                if (!string.IsNullOrEmpty(value))
                {
                    //Alias can't contain . (library exception)
                    selectors.Add(value + " as " + value.Replace(".", "_"));
                }
            }

            return selectors;
        }

        public static string TransformNestedSelectorsToString(string nestedSelectors, string nestedName, string aliasName)
        {
            return string.Concat(nestedName, ".Select(", nestedSelectors, ") as ", aliasName);
        }

        public static string TransformSelectorsToString(List<string> selectors)
        {
            //Return selector as expected by Linq.Dynamic.Core library
            return "new(" + string.Join(",", selectors.Distinct()) + ")";
        }

        public static string GetFinalResult(string parsedResult, List<string> selectors, Dictionary<string, string> mappings)
        {
            foreach (string s in selectors.OrderBy(s => s.Contains("_")))
            {
                string select = s;
                string[] selectAsSplitted = select.Split(" as ");

                string key = mappings.FirstOrDefault(x => x.Value.ToLowerInvariant().Equals(selectAsSplitted[0].Trim().ToLowerInvariant())).Key;

                if (!string.IsNullOrEmpty(key))
                {
                    //1
                    string[] aliasSelectorSplitted = selectAsSplitted[selectAsSplitted.Length - 1].Split("_");
                    //check if this a nested prop.
                    var np = Selectors.NestedProperties.FirstOrDefault(np => np.Key.ToLowerInvariant().Equals(aliasSelectorSplitted[0].ToLowerInvariant()));

                    if (np.Key != null)
                    {
                        string[] keySplitted = key.Split(".");
                        //1 - 1
                        parsedResult = parsedResult.Replace("\"" + selectAsSplitted[selectAsSplitted.Length - 1].Trim() + "\"", "\"" + keySplitted[keySplitted.Length - 1] + "\"");
                    }
                    else
                    {
                        //1
                        parsedResult = parsedResult.Replace("\"" + selectAsSplitted[selectAsSplitted.Length - 1].Trim() + "\"", "\"" + key + "\"");
                    }
                }
            }

            //Get object instead of string for treatment...
            JArray data = JsonConvert.DeserializeObject<JArray>(parsedResult);

            //Group same date Color.Id, Color.Code will be Color : { "Id": 1, "Code": "Test" }
            foreach (var record in data)
            {
                var filesProperties = record.Children<JProperty>()
                    .Where(p => p.Name.ToLowerInvariant().Contains("."));

                if (filesProperties.Any())
                {
                    List<string> groups = filesProperties.Select(p => p.Name.Split(".")[0]).Distinct().ToList();

                    //Color, Support for example
                    foreach (string group in groups)
                    {
                        dynamic fileProperty = new JObject();

                        foreach (var prop in filesProperties.Where(fp => fp.Name.ToLowerInvariant().Contains(group.ToLowerInvariant() + ".")).ToList())
                        {
                            fileProperty[prop.Name.Split(".")[1]] = prop.Value;
                            prop.Remove();
                        }

                        record[group] = fileProperty;
                    }
                }
            }

            return JsonConvert.SerializeObject(data);
        }
    }
}
