﻿using srvPrintshopIPEX.Domain.Entities;
using System;
using System.Linq.Expressions;

namespace srvPrintshopIPEX.Api.Helpers
{
    public static class QueryHelper
    {

        public static Expression<Func<PsNrbfileTemplating, bool>> GetNrbfileTemplatingMissingSpool()
        {
            return x => x.Nrbfile.PsSpool.Count != x.NombreSpools && (!x.DtAbandon.HasValue);
        }

        public static Expression<Func<PsNrbfileTemplating, bool>> GetNrbfileTemplatingMissingBonATirer()
        {
            return x => x.BonATirerATelecharger.HasValue && x.BonATirerATelecharger.Value;
        }

        public static Expression<Func<PsSpool, bool>> GetSpool(string externalReference, string externalContactAccount, int nrbFileId)
        {
            return x => x.ExternalReference == externalReference && x.ExternalContactAccount.ToLower().Equals(externalContactAccount.ToLower()) && x.NrbfileId == nrbFileId;
        }

        public static Expression<Func<PsSpool, bool>> GetSpool(string externalContactAccount, int nrbFileId)
        {
            return x => x.ExternalContactAccount.ToLower().Equals(externalContactAccount.ToLower()) && x.NrbfileId == nrbFileId;
        }

        public static Expression<Func<PsSpoolDetail, bool>> GetSpoolDetailsToDownload()
        {
            return x => x.DocumentATelecharger.HasValue && x.DocumentATelecharger.Value;
        }

        public static Expression<Func<Client, bool>> GetClient(string name)
        {
            return x => x.Name.ToLower().Equals(name.ToLower());
        }
    }
}
