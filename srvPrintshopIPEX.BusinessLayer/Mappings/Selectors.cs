﻿using System.Collections.Generic;

namespace srvPrintshopIPEX.BusinessLayer.Mappings
{
    public static class Selectors
    {
        //Nested properties whith name/alias
        public static readonly Dictionary<string, string> NestedProperties = new Dictionary<string, string>
        {
            { "PsSpoolDetail", "Files" }
        };
    }
}
