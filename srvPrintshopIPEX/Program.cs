﻿using Microsoft.Extensions.Configuration;
using System;
using System.IO;
using srvPrintshopIPEX.Domain.Configuration;
using Microsoft.Extensions.DependencyInjection;
using srvPrintshopIPEX.BusinessLayer.Core;
using srvPrintshopIPEX.Data;
using srvPrintshopIPEX.Data.UnitOfWork;
using Microsoft.EntityFrameworkCore;
using srvPrintshopIPEX.BusinessLayer.Services;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;
using Serilog;
using Serilog.Exceptions;

namespace srvPrintshopIPEX
{
    internal class Program
    {
        public static IPrintshopService _printshopService;
        public static Microsoft.Extensions.Logging.ILogger _logger;

        static async Task Main()
        {
            BuildConfiguration();
            _logger.LogInformation("Start Process");

            await _printshopService.ProcessXML();
            await _printshopService.DownloadBonsATirer();
            await _printshopService.DownloadFiles();

            _logger.LogInformation("End Process");
        }

        private static void BuildConfiguration()
        {
            var builder = new ConfigurationBuilder()
                    .SetBasePath(Directory.GetCurrentDirectory())
                    .AddJsonFile("config.json", optional: false);

            IConfiguration configuration = builder.Build();

            //serilog
            var serilogLogger = new LoggerConfiguration()
                .ReadFrom.Configuration(configuration)
                .Enrich.WithExceptionDetails()
                .CreateLogger();

            //config json
            SFTPConfiguration sFTPConfiguration = new();
            configuration.GetSection(nameof(SFTPConfiguration)).Bind(sFTPConfiguration);

            PathConfiguration pathConfiguration = new();
            configuration.GetSection(nameof(PathConfiguration)).Bind(pathConfiguration);

            var services = new ServiceCollection();
            services.AddSingleton<SFTPConfiguration>(sFTPConfiguration);
            services.AddSingleton<PathConfiguration>(pathConfiguration);
            services.AddScoped<IGenericReadOnlyService, GenericReadOnlyService>();
            services.AddScoped<IGenericService, GenericService>();
            services.AddScoped<IPrintshopService, PrintshopService>();
            services.AddTransient<IUnitOfWork, UnitOfWork>();
            services.AddDbContext<InforiusPrintSpoolerContext>(options => options.UseSqlServer(configuration.GetConnectionString("DefaultConnection")));
            services.AddLogging(configure => configure.ClearProviders().AddSerilog(logger: serilogLogger));
            ServiceProvider serviceProvider = services.BuildServiceProvider();

            _printshopService = serviceProvider.GetService<IPrintshopService>();
            _logger = serviceProvider.GetService<ILogger<Program>>();
        }
    }
}
